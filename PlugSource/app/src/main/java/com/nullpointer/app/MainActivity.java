package com.nullpointer.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.jar.Attributes;



public class MainActivity extends FragmentActivity {

    static final String root = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/");

    public void onClickLogin(View v) throws ExecutionException, InterruptedException {

        //Posting to validate_login.php with the entered information
        Poster usernameValidation = new Poster();
        String validateUserUrl = root + "validate_login.php";
        ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(2);
        userAndPasswordFields.add(new BasicNameValuePair("uname", input_uname));
        userAndPasswordFields.add(new BasicNameValuePair("password", input_password));
        JSONArray validationJArray = (JSONArray) usernameValidation.execute(validateUserUrl, userAndPasswordFields).get();

        try {
            //Check if the JSONArray has come back with a success or failure

            if (validationJArray.getJSONObject(0).getString("success").equals(new String("1")) ) {

                uname.setText(new String(""));
                password.setText(new String(""));

                Intent mapIntent = new Intent(this, MapActivity.class);
                mapIntent.putExtra("username",input_uname);
                startActivity(mapIntent);

            } else {
                Toast toast = new Toast(getApplicationContext());
                toast.makeText(MainActivity.this, validationJArray.getJSONObject(0).getString("message"), toast.LENGTH_SHORT).show();
            }
        }catch(JSONException e){
            e.printStackTrace();
        }catch (Exception e){ //If it is not JSON Exception it probably its the users internet connection
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(MainActivity.this, "Check your internet connection", toast.LENGTH_SHORT).show();
        }

    }

    public void onClickForgot(View v){

        Intent forgotPassIntent = new Intent (this, ForgotPasswordActivity.class);
        startActivity(forgotPassIntent);

    }

    public void onClickSignUp(View v){
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_account){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
}
