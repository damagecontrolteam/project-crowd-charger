package com.nullpointer.app;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.jar.Attributes;

public class ResetPasswordActivity extends Activity {

    static final String root = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/");

    public void onClickResetPassword(View v) throws ExecutionException, InterruptedException {


        Intent myIntent = getIntent();
        String username = myIntent.getStringExtra("username"); // will return "FirstKeyValue"


        //getting the input password1
        EditText apassword = (EditText) findViewById(R.id.editText);
        String input_apassword = apassword.getText().toString();

        //getting the input password2
        EditText anotherpassword = (EditText) findViewById((R.id.editText2));
        String input_anotherpassword = anotherpassword.getText().toString();


        if(input_apassword.length() == 0 || input_anotherpassword.length() == 0){
            Toast.makeText(getApplicationContext(),"You left a field blank, Oops!", Toast.LENGTH_SHORT).show();
            return;
        }



        if(input_apassword.equals(input_anotherpassword)) {
            //Posting to validate_login.php with the entered information
            Poster passwordReset = new Poster();
            String resetPassword = root + "reset_password.php";
            ArrayList<NameValuePair> passwordFields = new ArrayList<NameValuePair>(2);
            passwordFields.add(new BasicNameValuePair("uname", username));
            passwordFields.add(new BasicNameValuePair("apassword", input_apassword));
            JSONArray validationJArray = (JSONArray) passwordReset.execute(resetPassword, passwordFields).get();

            try {
                //Check if the JSONArray has come back with a success or failure

                if (validationJArray.getJSONObject(0).getString("success").equals(new String("1"))) {
                    Intent mainIntent = new Intent(this, MainActivity.class);

                    Toast toast = new Toast(getApplicationContext());
                    toast.makeText(ResetPasswordActivity.this, "Your password has been changed.", toast.LENGTH_SHORT).show();

                    startActivity(mainIntent);
                }
                else {
                    Toast toast = new Toast(getApplicationContext());
                    toast.makeText(ResetPasswordActivity.this, validationJArray.getJSONObject(0).getString("message"), toast.LENGTH_SHORT).show();
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            catch (Exception e) { //If it is not JSON Exception it probably its the users internet connection
                Toast toast = new Toast(getApplicationContext());
                toast.makeText(ResetPasswordActivity.this, "Check your internet connection", toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(ResetPasswordActivity.this, "Your passwords don't match.", toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
    }
}
