package com.nullpointer.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends Activity {

    public void onClickFinishSignUp(View v){

        //getting the input username
        EditText username_input = (EditText) findViewById(R.id.newUsernameField);
        String username = username_input.getText().toString();

        //getting the input username
        EditText email_input = (EditText) findViewById(R.id.emailField);
        String email = email_input.getText().toString();

        //password
        EditText password_input = (EditText) findViewById(R.id.passwordField);
        String password = password_input.getText().toString();

        //password confirmation
        EditText password_confirm_input = (EditText) findViewById(R.id.confirmField);
        String password_confirm = password_confirm_input.getText().toString();

        //ZIP
        EditText zip_input = (EditText) findViewById(R.id.zipField);
        String zip = zip_input.getText().toString();

        //Vehicle Make
        EditText vehicle_make_input = (EditText) findViewById(R.id.vehicleMakeField);
        String vehicle_make = vehicle_make_input.getText().toString();

        //vehicle Model
        EditText vehicle_model_input = (EditText) findViewById(R.id.vehicleModelField);
        String vehicle_model = vehicle_model_input.getText().toString();

        //vehicle Model
        EditText vehicle_year_input = (EditText) findViewById(R.id.vehicleYearField);
        String vehicle_year = vehicle_year_input.getText().toString();

        EditText income_input = (EditText) findViewById(R.id.income_field);
        String income = income_input.getText().toString();

        if(username.length() < 4){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "Your username must be at 4 characters in length", toast.LENGTH_LONG).show();
            return;
        }

        if(email.length() < 1){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "You must enter an email", toast.LENGTH_LONG).show();
            return;
        }
        if(!validEmail(email)){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "Please enter a valid email address", toast.LENGTH_LONG).show();
            return;
        }

        if(password.length() < 8){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "The password must be at least 8 characters in length", toast.LENGTH_LONG).show();
            return;
        }

        //Check if pass1 and pass2 are equal before going on
        if(!password.equals(password_confirm))
        {
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "Your passwords do not match :(", toast.LENGTH_LONG).show();
            return;
        }

        if(!isNumber(income) && income.length() > 0){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "Please enter a valid income", toast.LENGTH_LONG).show();
            return;
        }

        //Zip length checking
        if(zip.length() > 5 || (zip.length() < 5 && zip.length() > 0)){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "Please enter a valid zip code.", toast.LENGTH_LONG).show();
            return;
        }

        if(vehicle_year.length() > 4  || (vehicle_year.length() < 4 && vehicle_year.length() > 0)){
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SignUpActivity.this, "Please enter a valid vehicle year", toast.LENGTH_LONG).show();
            return;
        }


        //Posting to validate_login.php with the entered information
        Poster createNewUser = new Poster();
        String create_new_user_url = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/create_user.php");
        ArrayList<NameValuePair> create_new_user_post = new ArrayList<NameValuePair>(8);
        create_new_user_post.add(new BasicNameValuePair("uname", username));
        create_new_user_post.add(new BasicNameValuePair("password", password));
        create_new_user_post.add(new BasicNameValuePair("password2", password_confirm));
        create_new_user_post.add(new BasicNameValuePair("email", email));
        create_new_user_post.add(new BasicNameValuePair("zip", zip));
        create_new_user_post.add(new BasicNameValuePair("vmake", vehicle_make));
        create_new_user_post.add(new BasicNameValuePair("vmodel", vehicle_model));
        create_new_user_post.add(new BasicNameValuePair("vyear", vehicle_year));
        create_new_user_post.add(new BasicNameValuePair("income", income));


        try {
            JSONArray create_user_response = (JSONArray) createNewUser.execute(create_new_user_url, create_new_user_post).get();
            JSONObject responseObject = create_user_response.getJSONObject(0);

            String createUserSuccess = responseObject.getString("success");
            String createMessage = responseObject.getString("message");

            //remove for debuggin purposes
            //Log.d("singup", "Success: " + createUserSuccess);
            //Log.d("singup", "message: " + createMessage);


            if(createUserSuccess.equals("1")){
                //Added new behavior so that after user has finished signing up for account, they are taken directly to map
                Toast toast = new Toast(getApplicationContext());
                toast.makeText(SignUpActivity.this, createMessage, toast.LENGTH_LONG).show();

                Intent mapIntent = new Intent(this, MapActivity.class);
                mapIntent.putExtra("username",username);
                startActivity(mapIntent);

                finish();
                return;
            }else{
                Toast toast = new Toast(getApplicationContext());
                toast.makeText(SignUpActivity.this, createMessage, toast.LENGTH_LONG).show();
                return;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //check to see if the input string is a valid number
    public static boolean isNumber(String string) {
        try {
            Double.parseDouble(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    //check for valid email
    public static boolean validEmail (String string){
        return string.matches("[a-zA-Z0-9\\.]+@[a-zA-Z0-9\\-\\_\\.]+\\.[a-zA-Z0-9]{3}");
    }

}