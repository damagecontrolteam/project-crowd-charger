package com.nullpointer.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;


public class MapActivity extends Activity implements GoogleMap.OnInfoWindowClickListener {
    String sName = new String("Station Name");
    String sAddress = new String("Street Address");
    String sCity = new String("City");
    String sState = new String("State");
    String sZip = new String("ZIP");
    String sPhone= new String("Station Phone");
    String sStatusCode = new String("Status Code");
    String sLevel1 = new String("EV Level1 EVSE Num");
    String sLevel2 = new String("EV Level2 EVSE Num");
    String sDcFast = new String("EV DC Fast Count");
    String sOwnerType = new String("Owner Type Code");
    String sConnectorTypes = new String("EV Connector Types");
    String sLatitude = new String("Latitude");
    String sLongitude = new String("Longitude");


    String username = null;

    GoogleMap map; //map must be declared outside allow for non-initialized value
    ArrayList<HashMap<String, String>> jsonlist = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent myIntent = getIntent();
        username = myIntent.getStringExtra("username"); // will return "FirstKeyValue"


        //instantiate map fragments and enable location finder
        map = ( (MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMyLocationEnabled(true);



        //Once the google api has gotten a fix on the location, zoom in to it
        map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location myLocation) {


                //Zoom in to current location:
                LatLng myLatLng = new LatLng(myLocation.getLatitude(),
                        myLocation.getLongitude());
                CameraPosition myPosition = new CameraPosition.Builder()
                        .target(myLatLng).zoom(17).bearing(0).tilt(30).build();
                map.animateCamera(
                        CameraUpdateFactory.newCameraPosition(myPosition));
                map.setOnMyLocationChangeListener(null);
            }
        });


        map.setInfoWindowAdapter(new CustomMapPopUp(getLayoutInflater()));
        map.setOnInfoWindowClickListener(this);

}






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        new ProgressTask().execute();


        return true;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.action_account:
                onAccountSettings();
                break;

            case R.id.action_current:
                Location myLocation = map.getMyLocation();
                LatLng myLatLng = new LatLng(myLocation.getLatitude(),
                        myLocation.getLongitude());
                CameraPosition myPosition = new CameraPosition.Builder()
                        .target(myLatLng).zoom(17).bearing(0).tilt(30).build();
                map.animateCamera(
                        CameraUpdateFactory.newCameraPosition(myPosition));
                break;
            //add a new location
            case R.id.action_addExisting:
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {
                        double latval = point.latitude,
                                lonval = point.longitude;
                        MarkerOptions marker = new MarkerOptions().position(
                                new LatLng(point.latitude, point.longitude)).title("Existing Station"+ latval + lonval)
                                .draggable(false).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));;
                        map.addMarker(marker);
                        System.out.println(point.latitude + "---" + point.longitude);
                        map.setOnMapClickListener(null);  //This will turn off the listener
                        String LatPoint = Double.toString(latval),
                                LongPoint = Double.toString(lonval);
                        addExistingPoint(LatPoint, LongPoint);

                    }
                });
                break;
            //suggest a location
            case R.id.action_suggestLocation:
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {
                        double latval = point.latitude,
                                lonval = point.longitude;
                        MarkerOptions marker = new MarkerOptions().position(
                                new LatLng(point.latitude, point.longitude)).title("Suggested Station"+ latval + lonval)
                                .draggable(false)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                        map.addMarker(marker);
                        System.out.println(point.latitude + "---" + point.longitude);
                        map.setOnMapClickListener(null);
                        String LatPoint = Double.toString(latval),
                                LongPoint = Double.toString(lonval);
                        addSuggestedPoint(LatPoint, LongPoint);
                    }
                });
                break;


            case R.id.action_logOut:
                onClickLogOut();
                break;
            case R.id.action_filterOptions:
                onClickFilter();
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
    }

    public void onAccountSettings(){

        Intent settingsIntent = new Intent(this, AccountSettingsActivity.class);
        settingsIntent.putExtra("username",username);
        startActivity(settingsIntent);
    }



    @Override
    public void onInfoWindowClick(Marker marker) {//This is what gets executed when you click points
        Intent StationDisplayIntent = new Intent(this, StationDisplay.class);

        StationDisplayIntent.putExtra("latitude",Double.toString(marker.getPosition().latitude));
        StationDisplayIntent.putExtra("longitude",Double.toString(marker.getPosition().longitude));
        StationDisplayIntent.putExtra("username", username);
        startActivity(StationDisplayIntent);
    }

    // when log out is clicked, MapActivity will be closed and user is returned to the log in screen, back button will not return to map
    public void onClickLogOut() {
        System.exit(0);
        finish();
        Intent toMainIntent = new Intent(this, MainActivity.class);
        toMainIntent.putExtra("username",username);
        startActivity(toMainIntent);
    }


    public void onClickFilter() {
        finish();
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        settingsIntent.putExtra("username",username);
        startActivity(settingsIntent);

    }

    public void addSuggestedPoint(String Latitude, String Longitude){

        //Need to query all the information for the specific point were looking at
        Poster getSpecificPointData = new Poster();
        String validateUserUrl = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/add_points.php");

        ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(2);
        userAndPasswordFields.add(new BasicNameValuePair("latitude", Latitude));
        userAndPasswordFields.add(new BasicNameValuePair("longitude", Longitude));
        userAndPasswordFields.add(new BasicNameValuePair("status", "S"));


        String message = null;
        try {
            JSONArray stationJarray = (JSONArray) getSpecificPointData.execute(validateUserUrl, userAndPasswordFields).get();
            JSONObject station = stationJarray.getJSONObject(0);

            String success = station.getString("success");
             message = station.getString("message");

            Log.d("suggested", "Success: " + success);
            Log.d("suggested", "Message: " + message);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        Toast toast = new Toast(getApplicationContext());
        toast.makeText(MapActivity.this, message, toast.LENGTH_LONG).show();
    }

    public void addExistingPoint(String Latitude, String Longitude){

        //Need to query all the information for the specific point were looking at
        Poster getSpecificPointData = new Poster();
        String validateUserUrl = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/add_points.php");

        ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(2);
        userAndPasswordFields.add(new BasicNameValuePair("latitude", Latitude));
        userAndPasswordFields.add(new BasicNameValuePair("longitude", Longitude));

        userAndPasswordFields.add(new BasicNameValuePair("name", "User Submitted Charging Station - Might Not Actually Exist"));
        userAndPasswordFields.add(new BasicNameValuePair("status", "PE"));


        String message = null;
        try {
            JSONArray stationJarray = (JSONArray) getSpecificPointData.execute(validateUserUrl, userAndPasswordFields).get();
            JSONObject station = stationJarray.getJSONObject(0);

            String success = station.getString("success");
            message = station.getString("message");

            Log.d("suggested", "Success: " + success);
            Log.d("suggested", "Message: " + message);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        Toast toast = new Toast(getApplicationContext());
        toast.makeText(MapActivity.this, message, toast.LENGTH_LONG).show();
    }



    //The following is a seperate class for taking care of threaded networking
    private class ProgressTask extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPostExecute(final Boolean success) {


            for(HashMap<String, String> hash_object : jsonlist) {

                String StatusCode = hash_object.get(sStatusCode);

                //add existing points as green points
                if(StatusCode.equals("E")) {
                    map.addMarker(new MarkerOptions()
                                    .position(new LatLng(
                                            Double.parseDouble(hash_object.get(sLatitude)), Double.parseDouble(hash_object.get(sLongitude))
                                    ))
                                    //.title(hash_object.get(sName))
                                    .snippet(new String("Phone: " + hash_object.get(sPhone)) + "\n" +
                                            (new String("Address: " + hash_object.get(sAddress))) + "\n" +
                                            (new String("                     ") + new String(hash_object.get(sCity) + ", " + hash_object.get(sState))) + "\n" +
                                            (new String("# of Level 1: " + hash_object.get(sLevel1))) + "\n" +
                                            (new String("# of Level 2: " + hash_object.get(sLevel2))) + "\n" +
                                            (new String("# of DC Fast Chargers: " + hash_object.get(sDcFast))) + "\n" +
                                            (new String("Connector Types: " + hash_object.get(sConnectorTypes))) + "\n")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    );
                }

                //add planned points as blue points
                else if(StatusCode.equals("P")) {
                    map.addMarker(new MarkerOptions()
                                    .position(new LatLng(
                                            Double.parseDouble(hash_object.get(sLatitude)), Double.parseDouble(hash_object.get(sLongitude))
                                    ))
                                    //.title(hash_object.get(sName))
                                    .snippet(new String("Phone: " + hash_object.get(sPhone)) + "\n" +
                                            (new String("Address: " + hash_object.get(sAddress))) + "\n" +
                                            (new String("                     ") + new String(hash_object.get(sCity) + ", " + hash_object.get(sState))) + "\n" +
                                            (new String("# of Level 1: " + hash_object.get(sLevel1))) + "\n" +
                                            (new String("# of Level 2: " + hash_object.get(sLevel2))) + "\n" +
                                            (new String("# of DC Fast Chargers: " + hash_object.get(sDcFast))) + "\n" +
                                            (new String("Connector Types: " + hash_object.get(sConnectorTypes))) + "\n")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    );
                }

                //add suggested points as rose colored points
                else if(StatusCode.equals("S")) {
                    map.addMarker(new MarkerOptions()
                                    .position(new LatLng(
                                            Double.parseDouble(hash_object.get(sLatitude)), Double.parseDouble(hash_object.get(sLongitude))
                                    ))
                                    //.title(hash_object.get(sName))
                                    .snippet(new String("Phone: " + hash_object.get(sPhone)) + "\n" +
                                            (new String("Address: " + hash_object.get(sAddress))) + "\n" +
                                            (new String("                     ") + new String(hash_object.get(sCity) + ", " + hash_object.get(sState))) + "\n" +
                                            (new String("# of Level 1: " + hash_object.get(sLevel1))) + "\n" +
                                            (new String("# of Level 2: " + hash_object.get(sLevel2))) + "\n" +
                                            (new String("# of DC Fast Chargers: " + hash_object.get(sDcFast))) + "\n" +
                                            (new String("Connector Types: " + hash_object.get(sConnectorTypes))) + "\n")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                    );
                }
                //add suggested points as rose colored points
                else if(StatusCode.equals("PE")) {
                    map.addMarker(new MarkerOptions()
                                    .position(new LatLng(
                                            Double.parseDouble(hash_object.get(sLatitude)), Double.parseDouble(hash_object.get(sLongitude))
                                    ))
                                    //.title(hash_object.get(sName))
                                    .snippet(new String("Phone: " + hash_object.get(sPhone)) + "\n" +
                                            (new String("Address: " + hash_object.get(sAddress))) + "\n" +
                                            (new String("                     ") + new String(hash_object.get(sCity) + ", " + hash_object.get(sState))) + "\n" +
                                            (new String("# of Level 1: " + hash_object.get(sLevel1))) + "\n" +
                                            (new String("# of Level 2: " + hash_object.get(sLevel2))) + "\n" +
                                            (new String("# of DC Fast Chargers: " + hash_object.get(sDcFast))) + "\n" +
                                            (new String("Connector Types: " + hash_object.get(sConnectorTypes))) + "\n")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                    );
                }
            }//for adding all the marker


            Toast toast = new Toast(getApplicationContext());
            toast.makeText(MapActivity.this, "Finished Downloading Charging Stations", toast.LENGTH_SHORT).show();
        }




        protected Boolean doInBackground(final String... args) {
            JSONParser jParser = new JSONParser();
            StringBuffer url = new StringBuffer();
            url.append("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/get_all_point_info.php");
            url.append("?uname=").append(username);

            JSONArray json = jParser.getJSONFromUrl(url.toString());

            for (int i = 0; i < json.length(); i++) {
                try {

                    JSONObject c = json.getJSONObject(i);

                    //making map
                    HashMap<String, String> map = new HashMap<String, String>();


                    map.put(sName, c.getString(sName));
                    map.put(sAddress, c.getString(sAddress));
                    map.put(sCity, c.getString(sCity));
                    map.put(sState, c.getString(sState));
                    map.put(sZip, c.getString(sZip));
                    map.put(sPhone, c.getString(sPhone) );
                    map.put(sStatusCode, c.getString(sStatusCode));
                    map.put(sLevel1, c.getString(sLevel1));
                    map.put(sLevel2, c.getString(sLevel2));
                    map.put(sDcFast, c.getString(sDcFast));
                    map.put(sLatitude, c.getString(sLatitude) );
                    map.put(sLongitude, c.getString(sLongitude) );
                    map.put(sOwnerType, c.getString(sOwnerType));
                    map.put(sConnectorTypes, c.getString(sConnectorTypes));


                    jsonlist.add(map);

                } catch (JSONException e) {
                    Log.w("JSON", "E");
                    e.printStackTrace();

                }
            }
            return true;
        }
    }

}


