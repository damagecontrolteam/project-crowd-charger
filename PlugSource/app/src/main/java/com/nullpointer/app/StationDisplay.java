package com.nullpointer.app;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by jedan on 5/8/14.
 */
public class StationDisplay extends Activity {

    String sName = new String("Station Name");
    String sAddress = new String("Street Address");
    String sCity = new String("City");
    String sState = new String("State");
    String sZip = new String("ZIP");
    String sPhone= new String("Station Phone");
    String sStatusCode = new String("Status Code");
    String sLevel1 = new String("EV Level1 EVSE Num");
    String sLevel2 = new String("EV Level2 EVSE Num");
    String sDcFast = new String("EV DC Fast Count");
    String sOwnerType = new String("Owner Type Code");
    String sConnectorTypes = new String("EV Connector Types");
    String sLatitude = new String("Latitude");
    String sLongitude = new String("Longitude");

    String latitude = null;
    String longitude = null;
    String username = null;
    String stationId = null;
    String comment_id  = null;
    //String flagged = null;

    private static final String username_field = "Username";
    private static final String comment_field = "Comment";
    private static final String id_field = "Comment_ID";
    private static final String endorse_field = "Endorsements";
    //private static final String flagged_field = "Flagged";

    ArrayList<HashMap<String, String>> jsonlist = new ArrayList<HashMap<String, String>>();


    void putUpInfoOnStation(){
        TextView titleView = (TextView) findViewById(R.id.stationdisplaytitle);
        TextView generalEdView = (TextView) findViewById(R.id.stationdisplaygeneralinfo);

        //Need to query all the information for the specific point were looking at
        Poster getSpecificPointData = new Poster();
        String validateUserUrl = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/get_specific_point.php");

        ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(2);
        userAndPasswordFields.add(new BasicNameValuePair("latitude", latitude));
        userAndPasswordFields.add(new BasicNameValuePair("longitude", longitude));



        try {
            JSONArray stationJarray = (JSONArray) getSpecificPointData.execute(validateUserUrl, userAndPasswordFields).get();
            JSONObject station = stationJarray.getJSONObject(0);

            titleView.setText(station.getString(sName) );
            generalEdView.setText(
                    new String("Phone: " +  station.get(sPhone)) + "\n" +
                            (new String("Address: " + station.get(sAddress))) + "\n" +
                            (new String("                     ") + new String(station.get(sCity) + ", " + station.get(sState))) + "\n" +
                            (new String("# of EVSE(110V): " + station.get(sLevel1)))+ "\n" +
                            (new String("# of EVSE(J1772 connector): " + station.get(sLevel2)))+ "\n" +
                            (new String("# of DC Fast Chargers: " + station.get(sDcFast)))+ "\n" +
                            (new String("Connector Types: " + station.get(sConnectorTypes)))+ "\n");

            stationId = station.getString("P_ID");


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void putUpCommentFeed(){

        final ListView commentFeedView = (ListView) findViewById(R.id.commentfeed);

        commentFeedView.setOnItemClickListener(new ListView.OnItemClickListener() {
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popupMenu = new PopupMenu(StationDisplay.this, commentFeedView);
                popupMenu.getMenuInflater().inflate(R.menu.comment_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        String bob = parent.getItemAtPosition(position).toString();
                        HashMap<String, String> hashy = (HashMap<String, String>)parent.getItemAtPosition(position);
                        String hashy_id = hashy.get("Comment_ID");
                        TextView read_id = (TextView) findViewById(R.id.idLabel);
                        Poster getSpecificPointData = new Poster();
                        String validateUserUrl = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/flag_comment_id.php");
                        ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(1);
                        userAndPasswordFields.add(new BasicNameValuePair("Comment_ID", hashy_id));
                        try {
                            JSONArray stationJarray = (JSONArray) getSpecificPointData.execute(validateUserUrl, userAndPasswordFields).get();
                            Toast toast = Toast.makeText(getApplicationContext(), "The comment has been flagged.", Toast.LENGTH_SHORT);
                            toast.show();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                });
                popupMenu.show();



                /*String bob = parent.getItemAtPosition(position).toString();
                HashMap<String, String> hashy = (HashMap<String, String>)parent.getItemAtPosition(position);
                String hashy_id = hashy.get("Comment_ID");
                TextView read_id = (TextView) findViewById(R.id.idLabel);
                Poster getSpecificPointData = new Poster();
                String validateUserUrl = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/flag_comment_id.php");
                ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(1);
                userAndPasswordFields.add(new BasicNameValuePair("Comment_ID", hashy_id));
                try {
                    JSONArray stationJarray = (JSONArray) getSpecificPointData.execute(validateUserUrl, userAndPasswordFields).get();
                    Toast toast = Toast.makeText(getApplicationContext(), "The comment has been flagged.", Toast.LENGTH_SHORT);
                    toast.show();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }*/
                //String selected = ((TextView) view.findViewById(R.id.commentLabel)).getText().toString();

            }
        });

        commentFeedView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        jsonlist.clear();

        //Pulling the comments from the database
        String return_comments_url = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/return_station_comments.php");
        String query = new String(
                /*
                * Select * From StationComments
                * WHERE latitude - $latitude <= .001
                 * AND longitude - $longitude <= .001
                * */
                "Select * FROM  StationComments " +
                        "WHERE ABS(Latitude - " + latitude + ") <= 0.0001" +
                        "AND ABS(Longitude -" + longitude + ") <= 0.0001;"
        );
        ArrayList<NameValuePair> queryTuple = new ArrayList<NameValuePair>(1);
        queryTuple.add(new BasicNameValuePair("query", query));
        PullJSONUsingPost pullObject = new PullJSONUsingPost(return_comments_url,queryTuple);
        JSONArray commentJarray = pullObject.getJarray();

        ArrayList<String> comments = new ArrayList<String>();

        try{
            for(int i = 0; i < commentJarray.length(); i++) {
                JSONObject singleComment = commentJarray.getJSONObject(i);

                String in_username = singleComment.getString(username_field);
                String in_comment = singleComment.getString(comment_field);
                String in_id = singleComment.getString(id_field);

                HashMap<String, String> map = new HashMap<String, String>(3);

                map.put(username_field, in_username);
                map.put(comment_field, in_comment);
                map.put(id_field, in_id);

                jsonlist.add(map);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ListAdapter adapter;
        adapter = new SimpleAdapter(this, jsonlist, R.layout.list_activity, new String[] {username_field, comment_field, id_field}, new int[] {R.id.usernameLabel, R.id.commentLabel, R.id.idLabel});
        commentFeedView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stationdisplay);

        Intent myIntent = getIntent();
        latitude = myIntent.getStringExtra("latitude"); // will return "FirstKeyValue"
        longitude= myIntent.getStringExtra("longitude");
        username = myIntent.getStringExtra("username");

        //Fills the activity with the info on the station
        putUpInfoOnStation();

        //Fills the comment feed
        putUpCommentFeed();



    }


    public void onClickAddEndorsement(View v){
        String return_endorse_url = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/endorse_with_pid.php");

        ArrayList<NameValuePair> queryTuple = new ArrayList<NameValuePair>(2);
        queryTuple.add(new BasicNameValuePair("P_ID", stationId));
        queryTuple.add(new BasicNameValuePair("uname",username));


        PullJSONUsingPost pullObject = new PullJSONUsingPost(return_endorse_url,queryTuple);

        String message = null;
        try{
            JSONObject response = pullObject.getJarray().getJSONObject(0);
            String success = response.getString("success");
            message = response.getString("message");

            Log.d("endorse", "Success: " + success);
            Log.d("endorse" , "Message: " + message);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    //Communicates with php to add the comment to the database
    public void onClickAddComment(View v){

        EditText commentView = (EditText) findViewById(R.id.commentinput);
        String commentMessage = commentView.getText().toString();

        commentView.setText("");

        if(commentMessage.length() > 1000){
            Toast tooLong = new Toast(getApplicationContext());
            tooLong.makeText(this,"Your comment is too long",Toast.LENGTH_SHORT);
            return;
        }


        //Need to query all the information for the specific point were looking at
        Poster getSpecificPointData = new Poster();
        String validateUserUrl = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/create_comment.php");



        ArrayList<NameValuePair> userAndPasswordFields = new ArrayList<NameValuePair>(4); //5
        userAndPasswordFields.add(new BasicNameValuePair("latitude", latitude));
        userAndPasswordFields.add(new BasicNameValuePair("longitude", longitude));
        userAndPasswordFields.add(new BasicNameValuePair("comment", commentMessage));
        userAndPasswordFields.add(new BasicNameValuePair("username", username));
        //userAndPasswordFields.add(new BasicNameValuePair("flagged", flagged));



        try {
            JSONArray stationJarray = (JSONArray) getSpecificPointData.execute(validateUserUrl, userAndPasswordFields).get();
            JSONObject station = stationJarray.getJSONObject(0);

            Toast toast = new Toast(getApplicationContext());
            toast.makeText(StationDisplay.this, "Your comment has been added", toast.LENGTH_SHORT).show();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        putUpCommentFeed();


    }


}
