package com.nullpointer.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class AccountSettingsActivity extends Activity {

    JSONArray userJarray = null;
    JSONArray hasCar = null;
    String username = null;


    String user_username = null;
    String user_id = null;
    String user_email = null;
    String user_pw = null;
    String user_zip = null;
    String user_flagged=null;
    String account_type = null;
    String bad_comments = null;
    String user_income = null;

    //Top Labels
    TextView current_username_label = null;
    TextView current_email_label = null;
    TextView current_zip_label = null;
    TextView current_household_income = null;

    //Middle Password Changes
    EditText new_password_input = null;
    EditText new_password_confirm_input = null;

    //Main Info Changing
    EditText new_email_input = null;
    EditText new_zip_input = null;
    EditText new_household_income_input = null;


    private void pullUserData(){

        //Posting to validate_login.php with the entered information
        Poster getUserPost = new Poster();
        String return_users_url = "http://www.crowdcharger.dreamhosters.com/admin/crowdapp/returns_users.php";
        ArrayList<NameValuePair> queryPostInfo = new ArrayList<NameValuePair>(1);
        String query = new String("Select * FROM UserTable WHERE username='" + username + "';");
        queryPostInfo.add(new BasicNameValuePair("query", query));


        try {
            userJarray = (JSONArray) getUserPost.execute(return_users_url, queryPostInfo).get();
            JSONObject userInfo = userJarray.getJSONObject(0);

            user_username = userInfo.getString("username");
            user_id = userInfo.getString("UID");
            user_email = userInfo.getString("email");
            user_pw = userInfo.getString("PW");
            user_zip = userInfo.getString("ZIP");
            user_flagged = userInfo.getString("flagged");
            account_type = userInfo.getString("account_type");
            bad_comments = userInfo.getString("bad_comments");
            user_income = userInfo.getString("Income");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("p", userJarray.toString());
            e.printStackTrace();
        }

        current_username_label.setText(user_username);
        current_email_label.setText(user_email);
        current_zip_label.setText(user_zip);
        current_household_income.setText(user_income);

    }


    private void pullUserCarInformation(){
        /*
        //Posting to validate_login.php with the entered information
        Poster getUserPost = new Poster();
        String return_users_url = "http://www.crowdcharger.dreamhosters.com/admin/crowdapp/returns_vehicles_with_uid.php";

        ArrayList<NameValuePair> queryPostInfo = new ArrayList<NameValuePair>(1);
        queryPostInfo.add(new BasicNameValuePair("UID", user_id));

        JSONArray userDataJarray = null;
        try {
            userDataJarray = (JSONArray) getUserPost.execute(return_users_url, queryPostInfo).get();
            JSONObject userInfo = userDataJarray.getJSONObject(0);

            user_username = userInfo.getString("username");
            user_id = userInfo.getString("UID");
            user_email = userInfo.getString("email");
            user_pw = userInfo.getString("PW");
            user_zip = userInfo.getString("ZIP");
            user_flagged = userInfo.getString("flagged");
            account_type = userInfo.getString("account_type");
            bad_comments = userInfo.getString("bad_comments");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/

    }


    public void fillUSerInfo(){

    }

    private void getViewsById(){
        current_username_label = (TextView) findViewById(R.id.output_username);
        current_email_label = (TextView) findViewById(R.id.output_email);
        current_zip_label = (TextView) findViewById(R.id.output_zip);
        current_household_income = (TextView) findViewById(R.id.output_income);

        new_password_input = (EditText) findViewById(R.id.new_password_input_account_settings);
        new_password_confirm_input = (EditText) findViewById(R.id.new_password_confirmation_input_account_settings);

        new_email_input = (EditText) findViewById(R.id.email_input_account_settings);
        new_zip_input = (EditText) findViewById(R.id.zip_input_account_settings);
        new_household_income_input = (EditText) findViewById(R.id.household_income_input_account_settings);


    }


    public void onClickChangePassword(View v){
        String new_pass = new_password_input.getText().toString();
        String new_pass_confirmation = new_password_confirm_input.getText().toString();

        if(new_pass.equals(new_pass_confirmation) && new_pass.length() > 0){
            //we can do things
            //Posting to validate_login.php with the entered information
            Poster changePasswordPost = new Poster();
            String change_password_url = "http://www.crowdcharger.dreamhosters.com/admin/crowdapp/reset_password.php";

            ArrayList<NameValuePair> passChangePost = new ArrayList<NameValuePair>(1);
            passChangePost.add(new BasicNameValuePair("uname", username));
            passChangePost.add(new BasicNameValuePair("apassword", new_pass));

            JSONArray changePassword = null;

            try{
                changePassword = (JSONArray) changePasswordPost.execute(change_password_url, passChangePost).get();
                JSONObject response = userJarray.getJSONObject(0);

                Toast.makeText(getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT).show();
                user_pw = new_pass;

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Something went wrong, try again", Toast.LENGTH_SHORT).show();
            }


        }else if(new_pass.length() == 0 || new_pass_confirmation.length() == 0){
            //oopsies empty text
            Toast.makeText(getApplicationContext(), "At least one of your password fields are empty", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(), "Your passwords did not match", Toast.LENGTH_SHORT).show();
        }






    }


    public void onClickChangeInfo(View v){
        String new_email = new_email_input.getText().toString();
        String new_zip = new_zip_input.getText().toString();
        String new_household_income = new_household_income_input.getText().toString();


        //if there are things to be changed
        if(new_email.length() > 0 || new_zip.length() > 0 || new_household_income.length() > 0){

            Poster updateInfoPoster = new Poster();
            String updateInfoUrl = "http://www.crowdcharger.dreamhosters.com/admin/crowdapp/update_information.php";

            ArrayList<NameValuePair> updateInfoPost = new ArrayList<NameValuePair>(1);
            updateInfoPost.add(new BasicNameValuePair("uname", username));

            if(new_email.length() > 0){
                updateInfoPost.add(new BasicNameValuePair("email", new_email));
            }
            if(new_zip.length() > 0){
                updateInfoPost.add(new BasicNameValuePair("zip", new_zip));
            }
            if(new_household_income.length() > 0){
                updateInfoPost.add(new BasicNameValuePair("Income", new_household_income));
            }

            JSONArray updateInfoArray = null;

            try{
                updateInfoArray = (JSONArray) updateInfoPoster.execute(updateInfoUrl, updateInfoPost).get();
                JSONObject response = userJarray.getJSONObject(0);
                Toast.makeText(getApplicationContext(), "Your information has updated!", Toast.LENGTH_SHORT).show();
            } catch (Exception e){
                e.printStackTrace();
            }
        }else{
            Toast.makeText(getApplicationContext(), "No input detected", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        Intent myIntent = getIntent();
        username = myIntent.getStringExtra("username"); // will return "FirstKeyValue"
        getViewsById();
        //username_title.setText(username);

        pullUserData();



    }


}
