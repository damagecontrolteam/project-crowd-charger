package com.nullpointer.app;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.jar.Attributes;

public class ForgotPasswordActivity extends Activity {

    static final String root = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/");

    public void onClickForgotPassword(View v) throws ExecutionException, InterruptedException {

        //getting the input email
        EditText uemail = (EditText) findViewById(R.id.editText);
        String input_uemail = uemail.getText().toString();

        //getting the input username
        EditText uname = (EditText) findViewById(R.id.editText2);
        String input_uname = uname.getText().toString();

        //Posting to validate_login.php with the entered information
        Poster usernameValidation = new Poster();
        String validateUserUrl = root + "validate_email_uname.php";
        ArrayList<NameValuePair> userAndEmailFields = new ArrayList<NameValuePair>(2);
        userAndEmailFields.add(new BasicNameValuePair("uname", input_uname));
        userAndEmailFields.add(new BasicNameValuePair("uemail", input_uemail));
        JSONArray validationJArray = (JSONArray) usernameValidation.execute(validateUserUrl, userAndEmailFields).get();

        if(input_uemail.length() == 0 || input_uname.length() == 0){
            Toast.makeText(getApplicationContext(),"You left a blank field! Oops!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            //Check if the JSONArray has come back with a success or failure

            if (validationJArray.getJSONObject(0).getString("success").equals(new String("1")) ) {
                Intent SignInIntent = new Intent(this, MainActivity.class);
                startActivity(SignInIntent);
                finish();
            }
            else {
                Toast toast = new Toast(getApplicationContext());
                toast.makeText(ForgotPasswordActivity.this, validationJArray.getJSONObject(0).getString("message"), toast.LENGTH_SHORT).show();
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        catch (Exception e){ //If it is not JSON Exception it probably its the users internet connection
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(ForgotPasswordActivity.this, "Check your internet connection", toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }
}
