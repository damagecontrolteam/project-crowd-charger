package com.nullpointer.app;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class SettingsActivity extends Activity {
    //Create toggle objects that will be used to update filter options
    private ToggleButton ExistingPoints,
                         PlannedPoints,
                         SuggestedPoints,
                         CHADEMO,
                         J1772,
                         NEMA520,
                         NEMA1450,
                         NEMA515,
                         J1772COMBO,
                         TESLA,
                         PE;
    //Create the update button
    private Button Update;
    //declare the root of where the settings will be pulled
    static final String root = new String("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/");

    String username = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Intent myIntent = getIntent();
        username = myIntent.getStringExtra("username");

        addListenerOnButton();
    }

    public void addListenerOnButton() {

        ExistingPoints = (ToggleButton) findViewById(R.id.Existing);
        PlannedPoints = (ToggleButton) findViewById(R.id.Planned);
        SuggestedPoints = (ToggleButton) findViewById(R.id.Suggested);
        CHADEMO = (ToggleButton) findViewById(R.id.chademoToggle);
        J1772 = (ToggleButton) findViewById(R.id.J1772);
        NEMA520 = (ToggleButton) findViewById(R.id.NEMA520);
        NEMA1450 = (ToggleButton) findViewById(R.id.NEMA1450);
        NEMA515 = (ToggleButton) findViewById(R.id.NEMA515);
        J1772COMBO = (ToggleButton) findViewById(R.id.J1772Combo);
        TESLA = (ToggleButton) findViewById(R.id.TESLA);
        Update = (Button) findViewById(R.id.Update);
        PE = (ToggleButton) findViewById(R.id.PE);

        //initialize a parser to which will grab the data
        JSONBackgroundParser jParser = new JSONBackgroundParser();
        StringBuffer url = new StringBuffer();
        url.append("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/get_filters.php");
        url.append("?uname=").append(username);

        try {
            JSONArray filters = (JSONArray) jParser.execute(url.toString()).get();


            //Check the JSON ARRAY for the settings of each option
            if (filters.getJSONObject(0).getString("Existing").equals(new String("1")) ) {
                ExistingPoints.setChecked(true);
            } else {
                ExistingPoints.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("Planned").equals(new String("1")) ) {
                PlannedPoints.setChecked(true);

            } else {
                PlannedPoints.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("Suggested").equals(new String("1")) ) {
                SuggestedPoints.setChecked(true);
            } else {
                SuggestedPoints.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("PE").equals(new String("1")) ) {
                PE.setChecked(true);
            } else {
                PE.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("CHADEMO").equals(new String("1")) ) {
                CHADEMO.setChecked(true);
            } else {
                CHADEMO.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("J1772").equals(new String("1")) ) {
                J1772.setChecked(true);
            } else {
                J1772.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("NEMA520").equals(new String("1")) ) {
                NEMA520.setChecked(true);
            } else {
                NEMA520.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("NEMA1450").equals(new String("1")) ) {
                NEMA1450.setChecked(true);
            } else {
                NEMA1450.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("NEMA515").equals(new String("1")) ) {
                NEMA515.setChecked(true);
            } else {
                NEMA515.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("J1772Combo").equals(new String("1")) ) {
                J1772COMBO.setChecked(true);
            } else {
                J1772COMBO.setChecked(false);
            }

            if (filters.getJSONObject(0).getString("TESLA").equals(new String("1")) ) {
                TESLA.setChecked(true);
            } else {
                TESLA.setChecked(false);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }catch (Exception e){ //If it is not JSON Exception it probably its the users internet connection
            Toast toast = new Toast(getApplicationContext());
            toast.makeText(SettingsActivity.this, "Unable to obtain settings", toast.LENGTH_SHORT).show();
        }

        Update.setOnClickListener(updateFilterListener);

    }

    @Override
    public void onBackPressed() {
        updateFilterListener.onClick(new View(getApplicationContext()));

    }

    View.OnClickListener updateFilterListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            //Posting to validate_login.php with the entered information
            Poster filterPoster = new Poster();
            String filterUpdateUrl = root + "update_filters.php";

            ArrayList<NameValuePair> filterSettings = new ArrayList<NameValuePair>(12);
            filterSettings.add(new BasicNameValuePair("uname", username));
            filterSettings.add(new BasicNameValuePair("Planned", Boolean.toString( PlannedPoints.isChecked())));
            filterSettings.add(new BasicNameValuePair("Existing", Boolean.toString( ExistingPoints.isChecked())));
            filterSettings.add(new BasicNameValuePair("Suggested", Boolean.toString( SuggestedPoints.isChecked())));
            filterSettings.add(new BasicNameValuePair("CHADEMO", Boolean.toString( CHADEMO.isChecked())));
            filterSettings.add(new BasicNameValuePair("J1772", Boolean.toString( J1772.isChecked())));
            filterSettings.add(new BasicNameValuePair("NEMA520", Boolean.toString( NEMA520.isChecked())));
            filterSettings.add(new BasicNameValuePair("NEMA1450", Boolean.toString( NEMA1450.isChecked())));
            filterSettings.add(new BasicNameValuePair("NEMA515", Boolean.toString( NEMA515.isChecked())));
            filterSettings.add(new BasicNameValuePair("J1772COMBO", Boolean.toString( J1772COMBO.isChecked())));
            filterSettings.add(new BasicNameValuePair("TESLA", Boolean.toString( TESLA.isChecked())));
            filterSettings.add(new BasicNameValuePair("PE", Boolean.toString( PE.isChecked())));



            Log.d("boolean", Boolean.toString(PlannedPoints.isChecked()));

            try {
                JSONArray updateFilterResponse = (JSONArray) filterPoster.execute(filterUpdateUrl, filterSettings).get();
                JSONObject response = (JSONObject) updateFilterResponse.getJSONObject(0);

                Toast.makeText(SettingsActivity.this, response.getString("message").toString(),
                        Toast.LENGTH_SHORT).show();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            finish();
            goBackToMap();


        }

    };


    public void goBackToMap(){
        Intent settingsIntent = new Intent(this, MapActivity.class);
        settingsIntent.putExtra("username",username);
        startActivity(settingsIntent);
    }

}
