package com.nullpointer.app;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;


public class ListDataActivity extends ListActivity {

    private Context context;
    private static String url = "http://www.crowdcharger.dreamhosters.com/admin/crowdapp/get_users.php";

    private static String usr = "teamdamage";
    private static String pw = "Null Pointer Inc.";


    private static final String username_field = "username";
    private static final String email_field = "email";
    private static final String pw_field = "PW";
    private static final String zip_field = "ZIP";

    ArrayList<HashMap<String, String>> jsonlist = new ArrayList<HashMap<String, String>>();

    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data);
        new ProgressTask(ListDataActivity.this).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    //The Following is a Class======================================================================

    private class ProgressTask extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog dialog;


        public ProgressTask(ListActivity activity) {
            Log.i("1", "Called");
            context = activity;
            dialog = new ProgressDialog(context);
        }

        private Context context;

        protected void onPreExecute() {
            this.dialog.setMessage("Progress start");
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            /*if (dialog.isShowing()) {
                dialog.dismiss();
            }
            ListAdapter adapter = new SimpleAdapter(context,
                    jsonlist, R.layout.list_activity, new String[] { username_field, email_field, pw_field,zip_field },
                    new int[] { R.id.vehicleType, R.id.vehicleColor, R.id.fuel, R.id.fourth });

            setListAdapter(adapter);
            lv = getListView();*/
        }

        protected Boolean doInBackground(final String... args) {
            JSONParser jParser = new JSONParser();
            StringBuffer url = new StringBuffer();
            url.append("http://www.crowdcharger.dreamhosters.com/admin/crowdapp/get_all_point_info.php");
            url.append("?uname=").append("");

            JSONArray json = jParser.getJSONFromUrl(url.toString());

            for (int i = 0; i < json.length(); i++) {
                try {

                    JSONObject c = json.getJSONObject(i);

                    String in_uname = c.getString(username_field);
                    String in_efield = c.getString(email_field);
                    String in_pwfield = c.getString(pw_field);
                    String in_zipfield = c.getString(zip_field);

                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put(username_field, in_uname);
                    map.put(email_field, in_efield);
                    map.put(pw_field, in_pwfield);
                    map.put(zip_field, in_zipfield);

                    jsonlist.add(map);
                } catch (JSONException e)
                {
                    Log.w("JSON", "E");
                    e.printStackTrace();

                }
            }
            return null;

        }

    }



}
